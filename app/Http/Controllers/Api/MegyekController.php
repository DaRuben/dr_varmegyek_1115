<?php

namespace App\Http\Controllers\Api;
use App\Models\Megyek;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
class MegyekController extends Controller
{
    public function index()
    {
        $data = Megyek::orderBy('name')->get();
        $content = json_encode(['data' => $data, 'message' => 'Listed']);
        return response($content, Response::HTTP_OK);
    }

    public function create() {
        return view('megyek/create');
    }

    public function edit($id) {
        $entity = Megyek::find($id);

        return view('megyek/edit', ['entity' => $entity]);
    }

    public function update(Request $request, $id)
    {
        if ($id) {
            /** @var Megyek $entity */
            $entity = Megyek::find($id);
        }
        if (!$entity) {
            abort(404);
        }
        $entity = $this->setEntityData($entity, $request);
        $entity->update();

        return redirect(route('varmegye') . '#' . $entity->id);
    }

    public function delete(Request $request, $id)
    {
        /** @var Megyek $entity */
        $entity = Megyek::find($id);
        $entity?->delete();

        return response(json_encode(['data' => [], 'message' => 'Deleted']), Response::HTTP_GONE);
    }

    public function save(Request $request)
    {
        $id = $request->get('id');
        if ($id){
            $entity = Megyek::find($id);
            if (!$entity || $entity->isEmpty()){
                $content = json_encode(['data' => [], 'message' => 'Not found!']);
                return response($content, status:Response::HTTP_NOT_FOUND);
            }
            $entity = $this->setEntityData($entity, $request);
            $entity->update();
            $content = json_encode(['data' => $entity, 'message' => 'Updated!']);
            return response($content);
        }
        $entity = new Megyek();
        $entity = $this->setEntityData($entity, $request);
        $entity->save();
        $content = json_encode(['data' => $entity, 'message' => 'Created!']);
        return response($content, Response::HTTP_CREATED);
    }

    private function setEntityData(Megyek $entity, Request $request): ?Megyek
    {
        $entity->name = $request->get('name');

        return $entity;
    }

    private function getQuery()
    {
        return Megyek::select('*');
    }

    public function search(Request $request) {
        $needle = $request->get('needle');
        $entities = $this->getQuery()
            ->orWhere('name', 'like', "%{$needle}%")
            ->orderBy('name')
            ->get();
        if (!$entities || $entities->isEmpty()){
            return response(json_encode(['data' => $entities, 'message' => 'Not found!']), status:Response::HTTP_NOT_FOUND);
        }
        return response(json_encode(['data' => $entities, 'message' => 'Found!']), status:Response::HTTP_FOUND);

    }
}
