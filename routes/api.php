<?php

//use App\Http\Controllers\MegyekController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\MegyekController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('varmegye', [MegyekController::class, 'index'])->name(name:'apiMegyek');
Route::get('megyek/search', [MegyekController::class, 'search'])->name(name:'apiSearchMegyek');
Route::post('megyek/county', [MegyekController::class, 'save'])->name(name:'apiSaveMegyek');
Route::delete('megyek/{id}', [MegyekController::class, 'delete'])->name(name:'apiDeleteMegyek');
